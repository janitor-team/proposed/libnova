Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libnova

Files: *
Copyright: 1999-2000, Juan Carlos Remis
           2000-2005, Liam Girdwood <lgirdwood@gmail.com>
           2000-2005, 2009, 2011, 2015, Petr Kubánek <kubanek@fzu.cz>
           2015, Lukasz Komsta
License: LGPL-2+

Files: examples/transforms.c
       lntest/*.c
Copyright: 2000-2001, 2004, Liam Girdwood
           2008-2009, Petr Kubanek
License: GPL-2+

Files: m4/ax_check_compile_flag.m4
Copyright: 2008, Guido U. Draheim <guidod@gmx.de>
           2011, Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3+_AutoConfException

Files: git-version-gen
Copyright: 2007-2008, Free Software Foundation
License: GPL-3+

Files: debian/*
Copyright: 2007, Sune Vuorela <sune@debian.org>
           2011-2012, 2020, Pino Toscano <pino@debian.org>
           2016, 2019, Maximiliano Curia <maxy@debian.org>
License: GPL-2+

License: GPL-2+
 The complete text of the GNU General Public License version 2 can be found in
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 The complete text of the GNU General Public License version 3 can be found in
 `/usr/share/common-licenses/GPL-3'.

License: GPL-3+_AutoConfException
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
 .
 The complete text of the GNU General Public License version 3 can be found in
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2+
 The complete text of the GNU Library General Public License version 2 can be
 found in `/usr/share/common-licenses/LGPL-2'.
